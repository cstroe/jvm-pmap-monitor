# pmap monitoring on the JVM

## Motivation

One of the issues with the JVM is that getting true memory usage statistics is very hard.  The JVM provides metrics around the memory pools that it controls, which include: the JVM heap, the memory pools that it tracks. However, if you add up the usage of these pools, it doesn't equal the memory usage reported by the operating system.  There's a missing part, which we call "overhead" usage.

In essence, the true memory consumption of a java process should follow this equation:

```
RSS = JVM Heap Size + Off-heap Memory Pools Usage + "Overhead" Usage
```

This project aims to quantify that last piece, so that it can be tracked.

## Project Organization

This project has three components:

1. agent - This is a JVM agent which reads the JVM's process pmap and submits it to the pmap-web service
2. web - A web service that receives data from a pmap-agent
3. parser - A pmap parsing library that parses Linux pmap output into model objects
