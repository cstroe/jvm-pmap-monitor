# pmap-web

A web service to receive and parse pmap-agent requests.

## Developing

Start a test instance of postgres:

```
docker run --name pmap-db \
  -e POSTGRES_PASSWORD=pmap \
  -e PGDATA=/var/lib/postgresql/data/pgdata \
  -v pmap-db:/var/lib/postgresql/data \
  -p 5433:5432 \
  -d postgres
```
