CREATE SEQUENCE pmap_id_seq START 1 INCREMENT 1 NO MAXVALUE CACHE 1;

CREATE TABLE pmap (
  id          INTEGER PRIMARY KEY DEFAULT nextval('pmap_id_seq'),
  application VARCHAR(255) NOT NULL,
  pmap        TEXT NOT NULL,
  created     TIMESTAMP NOT NULL DEFAULT now()
);