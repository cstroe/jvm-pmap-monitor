package com.github.cstroe.pmap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PmapWebApplication {
    public static void main(String[] args) {
        SpringApplication.run(PmapWebApplication.class, args);
    }
}
