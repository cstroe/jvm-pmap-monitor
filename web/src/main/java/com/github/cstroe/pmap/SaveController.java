package com.github.cstroe.pmap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/save")
public class SaveController {
    private static Logger log = LoggerFactory.getLogger(SaveController.class);

    @Autowired
    private PmapRepository pmapRepository;

    @GetMapping
    public ResponseEntity<String> help() {
        return new ResponseEntity<>("Please use: <code>POST /save/{application}</code> with <code>Content-Type: text/plain</code>", HttpStatus.NOT_ACCEPTABLE);
    }

    @PostMapping(value = "/{application}", consumes = "text/plain")
    public ResponseEntity<String> savePmap(@PathVariable String application, @RequestBody String pmap) {
        Pmap newPmap = new Pmap();
        newPmap.setApplication(application);
        newPmap.setPmap(pmap);
        pmapRepository.save(newPmap);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
}
