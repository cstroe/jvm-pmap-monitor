package com.github.cstroe.pmap;

import lombok.Data;

import javax.annotation.Generated;
import javax.persistence.*;

@Entity
@Table(name = "pmap")
@Data
public class Pmap {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pmap_id")
    @SequenceGenerator(name = "pmap_id", sequenceName = "pmap_id_seq")
    private Integer id;

    @Column(name = "application")
    private String application;

    @Column(name = "pmap")
    private String pmap;
}
