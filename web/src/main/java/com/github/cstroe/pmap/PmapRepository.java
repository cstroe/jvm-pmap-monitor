package com.github.cstroe.pmap;

import org.springframework.data.repository.CrudRepository;

public interface PmapRepository extends CrudRepository<Pmap, Integer> {
}
