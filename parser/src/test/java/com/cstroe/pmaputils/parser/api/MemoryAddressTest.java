package com.cstroe.pmaputils.parser.api;

import org.junit.Test;

import static org.junit.Assert.*;

public class MemoryAddressTest {
    @Test
    public void testParse() {
        MemoryAddress address = MemoryAddress.parseHex("7f7fe2639000");
        assertEquals("", address.toString());
    }
}