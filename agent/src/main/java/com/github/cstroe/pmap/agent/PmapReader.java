package com.github.cstroe.pmap.agent;

import java.io.*;
import java.util.Optional;

import static java.lang.String.format;

public class PmapReader {
    public static Logger logger = new Logger(PmapReader.class.getName());

    public static Optional<String> read(int pid) {
        if(pid < 1) {
            return Optional.empty();
        }

        File file = new File("/proc/" + pid + "/maps");
        if(!(file.exists() && file.isFile() && file.canRead())) {
            logger.warning(format("File not found: %s", file.getName()));
            return Optional.empty();
        }

        try(FileInputStream inputStream = new FileInputStream(file);
            ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            byte[] buffer = new byte[1024];
            int bytesRead = -1;
            while((bytesRead = inputStream.read(buffer)) != -1) {
                baos.write(buffer, 0, bytesRead);
            }
            baos.flush();
            return Optional.of(baos.toString());
        } catch(FileNotFoundException ex) {
            logger.warning(format("File not found: %s", file.getName()));
        } catch(IOException ex) {
            logger.warning(format("Exception while accessing file: %s, %s", file.getName(), ex.getMessage()));
        }

        return Optional.empty();
    }
}
