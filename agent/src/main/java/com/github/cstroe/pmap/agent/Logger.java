package com.github.cstroe.pmap.agent;

import java.io.PrintStream;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

/**
 * @author <a href="mailto:cleclerc@cloudbees.com">Cyrille Le Clerc</a>
 */
public class Logger {

    public static Logger getLogger(String name) {
        return new Logger(name);
    }

    private final String name;

    public static PrintStream out;

    static {
        if ("stderr".equalsIgnoreCase(System.getenv("JMX_TRANS_AGENT_CONSOLE")) ||
                "stderr".equalsIgnoreCase(System.getProperty(Logger.class.getName() + ".console"))) {
            Logger.out = System.err;
        } else {
            Logger.out = System.out;
        }
    }

    public static Level level = Level.INFO;

    public static Level parseLevel(String level, Level defaultValue) {

        Map<String, Level> julLevelsByName = new HashMap<String, Level>() {
            {
                put("TRACE", Level.FINEST);
                put("FINEST", Level.FINEST);
                put("FINER", Level.FINER);
                put("FINE", Level.FINE);
                put("DEBUG", Level.FINE);
                put("INFO", Level.INFO);
                put("WARNING", Level.WARNING);
                put("WARN", Level.WARNING);
                put("SEVERE", Level.SEVERE);

            }
        };

        for(Map.Entry<String, Level> entry: julLevelsByName.entrySet()) {
            if(entry.getKey().equalsIgnoreCase(level))
                return entry.getValue();
        }
        return defaultValue;
    }

    static {
        //String level = System.getProperty(Logger.class.getName() + ".level", "INFO");
        String level = System.getProperty(Logger.class.getName() + ".level", "FINE");
        Logger.level = parseLevel(level, Level.INFO);
    }

    public Logger(String name) {
        this.name = name;
    }

    public void log(Level level, String msg) {
        log(level, msg, null);
    }

    public void log(Level level, String msg, Throwable thrown) {
        if (!isLoggable(level)) {
            return;
        }
        Logger.out.println(new Timestamp(System.currentTimeMillis()) + " " + level + " [" + Thread.currentThread().getName() + "] " + name + " - " + msg);
        if (thrown != null) {
            thrown.printStackTrace(Logger.out);
        }
    }

    public void finest(String msg) {
        log(Level.FINEST, msg);
    }

    public void finer(String msg) {
        log(Level.FINER, msg);
    }

    public void fine(String msg) {
        log(Level.FINE, msg);
    }

    public void info(String msg) {
        log(Level.INFO, msg);
    }

    public void warning(String msg) {
        log(Level.WARNING, msg);
    }

    public boolean isLoggable(Level level) {
        if (level.intValue() < this.level.intValue()) {
            return false;
        }
        return true;
    }
}