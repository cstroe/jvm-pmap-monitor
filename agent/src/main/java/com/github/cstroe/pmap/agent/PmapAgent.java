package com.github.cstroe.pmap.agent;

import java.lang.instrument.Instrumentation;
import java.lang.management.ManagementFactory;
import java.util.logging.Level;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PmapAgent {
    private static Logger logger = Logger.getLogger(PmapAgent.class.getName());

    private PmapAgent(){}

    public static void agentmain(String configFile, Instrumentation inst) {
        initializeAgent();
    }

    public static void premain(final String configFile, Instrumentation inst) {
        initializeAgent();
    }

    public static void main(String[] args) {
        Logger.out.println(getVersionInfo());
    }

    public static int getPid() {
        String runtimeName = ManagementFactory.getRuntimeMXBean().getName();

        Pattern p = Pattern.compile("([0-9]+)@[a-zA-Z0-9]+");
        Matcher matcher = p.matcher(runtimeName);

        if(matcher.matches()) {
            MatchResult result = matcher.toMatchResult();
            String pid = result.group(1);
            return Integer.parseInt(pid);
        } else {
            return -1;
        }
    }

    private static void initializeAgent() {
        try {
            logger.info("Starting '" + getVersionInfo() + " ...");
            PmapHttpExporter exporter = new PmapHttpExporter();
            exporter.start();
            logger.info("PmapAgent started");
        } catch (Exception e) {
            String msg = "Exception starting PmapHttpExporter";
            logger.log(Level.SEVERE, msg, e);
            throw new IllegalStateException(msg, e);
        }
    }


    public static String getVersionInfo() {
        Package pkg = PmapAgent.class.getPackage();
        if (pkg == null) {
            return "pmap-agent";
        } else {
            return pkg.getImplementationTitle() + ": " + pkg.getImplementationVersion();
        }
    }
}
