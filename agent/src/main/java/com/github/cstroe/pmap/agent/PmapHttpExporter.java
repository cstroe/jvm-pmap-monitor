package com.github.cstroe.pmap.agent;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;

import static java.lang.String.format;

public class PmapHttpExporter {
    private final Logger logger = Logger.getLogger(getClass().getName());

    private ThreadFactory threadFactory = new ThreadFactory() {
        final AtomicInteger counter = new AtomicInteger();

        @Override
        public Thread newThread(Runnable r) {
            Thread thread = Executors.defaultThreadFactory().newThread(r);
            thread.setDaemon(true);
            thread.setName("pmap-exporter-" + counter.incrementAndGet());
            return thread;
        }
    };

    private ScheduledExecutorService scheduledExecutorService;
    private ScheduledFuture scheduledFuture;
    private volatile long runIntervalSeconds;
    private int pid;
    private URL url;
    private int connectTimeoutMillis = 3000;
    private int readTimeoutMillis = 5000;
    private List<String> batchedPmaps = new ArrayList<>();

    public PmapHttpExporter() {
        this.runIntervalSeconds = 10;
        this.pid = PmapAgent.getPid();
        this.url = buildURL();
        logger.info(format("Detected pid: %d", pid));
    }

    public void start() {
        if (logger.isLoggable(Level.FINER)) {
            logger.fine("starting " + this.toString() + " ...");
        } else {
            logger.fine("starting " + getClass().getName() + " ...");
        }

        if (scheduledExecutorService != null || scheduledFuture != null)
            throw new IllegalArgumentException("Exporter is already started: scheduledExecutorService=" + scheduledExecutorService + ", scheduledFuture=" + scheduledFuture);

        scheduledExecutorService = Executors.newScheduledThreadPool(1, threadFactory);

        scheduledFuture = scheduledExecutorService.scheduleWithFixedDelay(this::collectAndExport,
                runIntervalSeconds / 2, runIntervalSeconds, TimeUnit.SECONDS);

        logger.fine(getClass().getName() + " started");
    }

    public void stop() {
        // cancel jobs
        if (scheduledFuture != null) {
            scheduledFuture.cancel(true);
            scheduledFuture = null;
        }
        scheduledExecutorService.shutdown();

        // one last export
        collectAndExport();

        // wait for stop
        try {
            scheduledExecutorService.awaitTermination(runIntervalSeconds, TimeUnit.MILLISECONDS);

        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
        scheduledExecutorService = null;

        logger.info(getClass().getName() + " stopped.");
    }

    private void collectAndExport() {
        try {
            Optional<String> pmap = PmapReader.read(pid);
            if (pmap.isPresent()) {
                HttpURLConnection conn = null;
                try {
                    conn = createAndConfigureConnection();
                    sendPmap(pmap.get(), conn);
                    logger.info("Sent pmap.");
                } catch (IOException ioe) {
                    logger.warning("Could not send pmap: " + ioe.getMessage());
                    //} finally {
                    //    closeQuietly(conn);
                }
            } else {
                logger.warning("Could not read pmap.  Pid: " + pid);
            }
        } catch(Exception ex) {
            logger.warning("Caught exception: " + ex.getMessage());
            // do nothing
        }
    }

    @Override
    public String toString() {
        return "PmapHttpExporter";
    }

    private URL buildURL() {
        try {
            return new URL("http://localhost:8080/save/test");
        } catch (MalformedURLException ex) {
            logger.warning("Could not create URL: " + ex.getMessage());
            return null;
        }
    }

    private HttpURLConnection createAndConfigureConnection() throws ProtocolException {
        HttpURLConnection conn = openHttpConnection();
        conn.setConnectTimeout(connectTimeoutMillis);
        conn.setReadTimeout(readTimeoutMillis);
        conn.setRequestProperty("Content-Type", "text/plain");
        conn.setDoOutput(true);
        conn.setRequestMethod("POST");
        return conn;
    }

    private HttpURLConnection openHttpConnection() {
        try {
            return (HttpURLConnection) url.openConnection();
        } catch (IOException | ClassCastException e) {
            throw new RuntimeException("Failed to create HttpURLConnection to " + url + " - is it a valid HTTP url?",
                    e);
        }
    }

    private void sendPmap(String pmap, HttpURLConnection conn) throws IOException {
        writePmap(conn, pmap);
        int responseCode = conn.getResponseCode();
        if (responseCode / 100 != 2) {
            String message = "Failed to write metrics, response code: " + responseCode
                    + ", response message: " + conn.getResponseMessage();
            logger.warning(message);
            throw new RuntimeException(message);
        }
        String response = readResponse(conn);
        logger.log(Level.FINE, "Response from pmap-web: " + response);
    }

    private void writePmap(HttpURLConnection conn, String pmap)
            throws UnsupportedEncodingException, IOException {
        byte[] toSendBytes = pmap.getBytes("UTF-8");
        conn.setRequestProperty("Content-Length", Integer.toString(toSendBytes.length));
        try (OutputStream os = conn.getOutputStream()) {
            os.write(toSendBytes);
            os.flush();
        }
    }

    private String readResponse(HttpURLConnection conn) throws IOException, UnsupportedEncodingException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (InputStream is = conn.getInputStream()) {
            copy(is, baos);
        }
        String response = new String(baos.toByteArray(), "UTF-8");
        return response;
    }

    public static void copy(InputStream in, OutputStream out) throws IOException{
        byte[] buffer = new byte[1024];
        int length;
        while ((length = in.read(buffer)) >= 0) {
            out.write(buffer, 0,length);
        }
    }

    public static void closeQuietly (URLConnection cnn) {
        if (cnn == null) {
            return;
        } else if (cnn instanceof HttpURLConnection) {
            ((HttpURLConnection) cnn).disconnect();
        } else {
            // do nothing
        }
    }
}
