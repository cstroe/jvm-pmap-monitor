package com.github.cstroe.pmap.agent;

import org.junit.Test;

import static org.junit.Assert.*;

public class PmapAgentTest {
    @Test
    public void pidDiscovery() {
        assertTrue(PmapAgent.getPid() > 0);
    }
}