package com.github.cstroe.pmap.agent;

import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PmapReaderTest {
    @Test
    public void readTest() {
        Optional<String> pmapString = PmapReader.read(PmapAgent.getPid());
        assertTrue(pmapString.isPresent());
        String pmap = pmapString.get();
        assertFalse(pmap.isEmpty());
        assertTrue(pmap.contains("java"));
    }
}